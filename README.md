
pTester - PHP Performance Test tool
====================================================

@utor: Moisés Alcocer, 2017
@web: https://www.ironwoods.es

Librarie for run performance tests in PHP developed from previous PHP
Performance Test tool developed in 2014.
Original version is available in: https://github.com/oricis/PHP-Performance-Test
