<?php namespace ironwoods\tools\ptester\classes;
/**
 * @file: runsizer.php
 * @info: Class for test total memory consumption in a running process
 *
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */


final class RunSizer {

	/**************************************************************************/
	/*** Properties declaration ***********************************************/

		private static $class = "RunSizer";

		private static $stats = NULL;


	/**************************************************************************/
	/*** Methods declaration **************************************************/

	/*** Public Methods ***************/

		/**
		 * Sets initial datas
		 *
		 */
		public static function begin() {

			DataMarks::init();
			DataMarks::setDatas();
		}

		/**
		 * Gets stats
		 *
		 * @return string
		 */
		public static function getStats() {

			return self::$stats;
		}

		/**
		 * Sets final datas
		 *
		 * @param  boolean		$show_stats
		 */
		public static function stop( $show_stats=FALSE ) {

			DataMarks::setDatas();
			self::$stats = Stats::getForRunSizer();
			unset( $GLOBALS[ "pTester" ]);

			if ( $show_stats ) {
				die( self::$stats );
				exit();
			}
		}


	/*** Private Methods **************/



} //class
