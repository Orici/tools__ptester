<?php namespace ironwoods\tools\ptester\classes;
/**
 * @file: datamarks.php
 * @info: Class to store datas
 *
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */


final class DataMarks {

	/**************************************************************************/
	/*** Properties declaration ***********************************************/

		private static $class = "DataMarks";


	/**************************************************************************/
	/*** Methods declaration **************************************************/

	/*** Public Methods ***************/

		public static function getFirstTime() {

			return ( isset( $GLOBALS[ "pTester" ][ "time-marks" ][ 0 ]))
				? $GLOBALS[ "pTester" ][ "time-marks" ][ 0 ]
				: 0;
		}

		public static function getLastTime() {
			$total_times = count( $GLOBALS[ "pTester" ][ "time-marks" ]);

			if ( $total_times ) {
				$total_times -= 1;

				return $GLOBALS[ "pTester" ][ "time-marks" ][ $total_times ];
			}
			

			return 0;	
		}

		/**
		 * Gets the diference of memory consumption between the first and 
		 * the last measurements
		 * 
		 * @return int
		 */
		public static function getMemoryDiff() {
			//prob( self::$class . " / setTime()" );

			$memory_marks = count( $GLOBALS[ "pTester" ][ "memory-marks" ]);
			$pos = $memory_marks - 1;

			$first = $GLOBALS[ "pTester" ][ "memory-marks" ][0];
			$last  = $GLOBALS[ "pTester" ][ "memory-marks" ][ $pos ];
			//dx( $GLOBALS[ "pTester" ][ "memory-marks" ], TRUE );

			return $last - $first;
		}

		/**
		 * Initializes the arrays for data
		 * 
		 */
		public static function init() {

			//Erases previous datas
			if (isset( $GLOBALS[ "pTester" ]))
				unset( $GLOBALS[ "pTester" ]);

			$GLOBALS[ "pTester" ] = array();

			/*
				$GLOBALS[ "pTester" ] = [
					[ "memory-marks" ][0] = 45,
					[ "time-marks" ][0]	  = 34,
				];
			*/
		}

		/**
		 * Sets a quantity of memory in the array and the time of the 
		 * measurement
		 * 
		 */
		public static function setDatas() {
			//prob( self::$class . " / setDatas()" );

			$GLOBALS[ "pTester" ][ "time-marks" ][]   = time();
			$GLOBALS[ "pTester" ][ "memory-marks" ][] = memory_get_usage();
			//memory_get_usage() -> returns bytes
		}


	/*** Private Methods **************/



} //class
