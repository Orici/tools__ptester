<?php namespace ironwoods\tools\ptester\classes;
/**
 * @file: __template.php
 * @info: Class for ...
 *
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */


final class Xxx {

	/**************************************************************************/
	/*** Properties declaration ***********************************************/

		private static $class = "Xxx";


	/**************************************************************************/
	/*** Methods declaration **************************************************/

	/*** Public Methods ***************/

		/**
		 * Shows stats of script executions and ends the execution
		 *
		 */
		public static function xxx() {

		}


	/*** Private Methods **************/



} //class
