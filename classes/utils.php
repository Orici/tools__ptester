<?php namespace ironwoods\tools\ptester\classes;
/**
 * @file: utils.php
 * @info: Class with useful methods
 *
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */


final class Utils {

	/**************************************************************************/
	/*** Properties declaration ***********************************************/

		private $class = "Utils";


	/**************************************************************************/
	/*** Methods declaration **************************************************/

	/*** Public Methods ***************/

		/**
		 * Gets an average value from a group of values
		 * From: ACM Framework / helpers/ Aritmethica
		 *
		 * @param  array 		$arr_numbers
		 * @return int
		 */
		public static function getAverage( $arr_numbers ) {

			if ( is_array( $arr_numbers )) {

				$len   = count( $arr_numbers );
				$total = array_sum( $arr_numbers );
				

				return $total / $len;
			}


			return FALSE;
		}

		/**
		 * Allows to get maximum amount of memory a script may consume, in 
		 * bytes, setted in "php.ini"
		 *
		 * @return int
		 */
		public static function getAvailableMemory() {

			//Gets value from file: "php.ini", usually an amount of Mb
			$value = @ini_get( 'memory_limit' ); //Example: 128M

			//Limit in Mb
			if ( preg_match( '/[0-9]+m/i', $value ))
				return ((int) $value ) * 1024 * 1024;
			
			
			die( "Memory limit don't setted in Mb -> revise your \"php.ini\"" );
		}

		/**
		 * Gets a value of memory with its units
		 *
		 * @param  int 			$memory_bytes
		 * @return string
		 */
		public static function getValueAndUnits( $memory_bytes ) { 

			if ( $memory_bytes < 1024 ) 
				return "{$memory_bytes} bytes.";

			if ( $memory_bytes < 1048576 ) //1024*1024
				return round(( $memory_bytes / 1024 ), 3 ) . " kilobytes."; 


			return round(( $memory_bytes / 1048576 ), 3 ) . " megabytes."; 
		} 


	/*** Private Methods **************/



} //class
