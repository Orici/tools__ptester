<?php namespace ironwoods\tools\ptester\classes;
/**
 * @file: stats.php
 * @info: Class for ...
 *
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */


final class Stats {

	/**************************************************************************/
	/*** Properties declaration ***********************************************/

		private static  $class = "Stats";

		private static $tested_method = 1;


	/**************************************************************************/
	/*** Methods declaration **************************************************/

	/*** Public Methods ***************/

		/**
		 * Gets a message with stats about the performance in the sized interval
		 * 
		 * @param  boolean		$diff_memory
		 * @return string
		 */
		public static function get( $diff_memory=FALSE ) {
			prob( self::$class . " / get()" );

			$time_format = "d-m-Y H:i:s";
			
			$arr_data	= $GLOBALS[ "pTester" ];
			$begin_time = DataMarks::getFirstTime();
			$end_time	= DataMarks::getLastTime();
			$waiting	= ( $end_time - $begin_time );

			$memory_used = ( $diff_memory )
				? DataMarks::getMemoryDiff()
				: Utils::getAverage( $arr_data[ "memory-marks" ]);
			$memory_used = Utils::getValueAndUnits( $memory_used );

			$str = "Test of performance in a interval<br>";
			$str .= "================================================== <br>";
			$str .= "Begin at " . date( $time_format, $begin_time ) . "<br>";
			$str .= "<br>";
			$str .= "Memory used: {$memory_used} <br>";
			$str .= "================================================== <br>";
			$str .= "End at " . date( $time_format, $end_time ) . "<br>";
			$str .= "Interval of " . $waiting . " seconds.";


			return $str;
		}

		/**
		 * Gets a message with stats about the performance in the sized interval
		 * of running process
		 * 
		 * @return string
		 */
		public static function getForRunSizer() {

			return self::get( TRUE );
		}

		/**
		 * Calcs the averages and shows stats
		 *
		 * @param  array 	$arr_times
		 * @param  array 	$arr_memory_usages
		 * @param  int 		$number_of_tests
		 */
		public static function showResults( 
			$arr_times=NULL, 
			$arr_memory_usages=NULL, 
			$number_of_tests=NULL ) 
		{

			if ( $arr_times && $arr_memory_usages && $number_of_tests 
				&& is_array( $arr_times ) && is_array( $arr_memory_usages )
				&& ((int) $number_of_tests ) > 1 ) {

				$used_time   = array_sum( $arr_times ) / $number_of_tests;
				$used_memory = Utils::getValueAndUnits(
					array_sum( $arr_memory_usages ) / $number_of_tests 
				);
				
				prob( "Tested method number: " . self::$tested_method );
				prob( "Used time: {$used_time} miliseconds" );
				prob( "Used memory: {$used_memory}<hr><br>" );
				
				self::$tested_method++;

			} else
				die( self::$class . " / showResults() -> Err args" );
		}


	/*** Private Methods **************/



} //class
