<?php namespace ironwoods\tools\ptester\classes;
/**
 * @file: intervalsizer.php
 * @info: Class to size performance in time intervals
 *
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package	ironwoods.tools.pTester
 */


final class IntervalSizer {

	/**************************************************************************/
	/*** Properties declaration ***********************************************/

		private static $class = "IntervalSizer";

		private static $measurements_by_second  = 20;
		private static $waiting = 2; //seconds (max time execution)	
		

	/**************************************************************************/
	/*** Methods declaration **************************************************/

	/*** Public Methods ***************/

		/**
		 * Begins the time interval
		 * 
		 */
		public static function begin() {
			prob( self::$class . " / begin()" );

			//Initializes the arrays for data
			DataMarks::init();

			//Runs the sized interval
			self::run();
		}

		/**
		 * Ends the time interval and show the stats of performance
		 * 
		 */
		public static function end() {
			prob( self::$class . " / end()" );

			//Stores the end marks: time and memory usage
			DataMarks::setDatas();

			//Gets stats, prints and finalize the script execution
			$res = Stats::get();
			die( $res ); 
			exit();
		}


	////////////////////////////////////////////////////////////////////////////
	/// Setters
	/// 
	
		/**
		 * Sets new value for the propertie "$cycles"
		 * 
		 * @param int 		$number
		 */
		public static function setMeasurementsBySecond( $number ) {

			if ((int) $number > 0 )
				self::$measurements_by_second = (int) $number;
		}

		/**
		 * Sets new value for the propertie "$waiting"
		 * 
		 * @param int 		$seconds
		 */
		public static function setTime( $seconds ) {

			if ((int) $seconds > 0 )
				self::$waiting = (int) $seconds;
		}


	/*** Private Methods **************/

		private static function run() {
			prob( self::$class . " / run()" );

			//Stores firsts data (time mark and used memory)
			DataMarks::setDatas();
			//dx( $GLOBALS[ "pTester" ]); //trace

			$final_time = $GLOBALS[ "pTester" ][ "time-marks" ][ 0 ]
				+ ( self::$waiting * 1000 );

			$waiting = self::$waiting * self::$measurements_by_second;
			$time_interval = 1000000 / self::$measurements_by_second;
			$now = time();

			
			dx( "\$waiting: " . $waiting 
				. "<br>\$time_interval: " . $time_interval
				. "<br>\$final_time: " . $final_time
				. "<br>\$now: " . $now
			); //trace

			while (( $waiting > 0 ) && ( $final_time >= $now )) {
				//prob( "while() -> \$waiting: " . $waiting );

				usleep( $time_interval );
				//Stores data (time mark and used memory)
				DataMarks::setDatas();

				$now = time();
				$waiting--;
			}
			//dx( $GLOBALS[ "pTester" ]); //trace
			//dx( "fin", TRUE );

			self::end();
		}
		

} //class

/**
 * sleep(miliseconds)
 * usleep(microseconds)
 *
 * sleep( 1 ) === usleep( 1000000 )
 *
 * https://www.w3schools.com/php/func_misc_sleep.asp
 */
