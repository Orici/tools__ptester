<?php namespace ironwoods\tools\ptester\classes;
/**
 * @file: performancetest.php
 * @info: Class to test performance of classes and methods ---> Cals average 
 * time and memory used
 * 
 * Copyright 2014, Moisés Alcocer
 * <m_alc1@hotmail.com>
 * https://www.ironwoods.es
 */



final class PerformanceTest {
	
	/**************************************************************************/
	/*** Properties declaration ***********************************************/

		private static $class = "PerformanceTest";

		private static $tested_method = 1;
		private static $number_of_cycles = 1000;

		//Number of tests to run to get the average stats
		private static $number_of_tests = 100;

		//Uses or not "runCycles()" to wide used time
		private static $waiting = FALSE;

		private static $arr_neccesary_times = array();
		private static $arr_memory_usages   = array();


	/**************************************************************************/
	/*** Methods declaration **************************************************/

	/*** Public Methods ***************/

		/**
		 * Tests memory usage and running time for the functions or methods
		 * 
		 * @param  array		$arr_func_or_methods
		 */
		public static function test( $arr_func_or_methods ) {
			prob( self::$class . " / test()<br>" );

			if ( $arr_func_or_methods && is_array( $arr_func_or_methods )) {
				
				/**
				 * Runs tests and sets the data for stats
				 *
				 */
				$cont = 0;
				while ( $cont < self::$number_of_tests ) {

					foreach ( $arr_func_or_methods as $i => $func_or_method ) {

						self::getMeasurements( $func_or_method, $i, $cont );
					}
					

					$cont++;
				}
				
				
				/**
				 * Shows stats of running tests
				 *
				 */
				$number_of_running_tests = count( $arr_func_or_methods );

				$cont = 0;
				while ( $cont < $number_of_running_tests ) {

					Stats::showResults(
						self::$arr_neccesary_times[ $cont ], 
						self::$arr_memory_usages[ $cont ], 
						self::$number_of_tests
					);
					
					$cont++;
				}

			} else
				die( self::$class . " / showResults() -> Err args" );
		}


	////////////////////////////////////////////////////////////////////////////
	/// Setters
	/// 
		
		/**
		 * Sets new value for the properties "$waiting" and "$number_of_cycles"
		 * 
		 * @param  boolean		$waiting
		 * @param  int 			$number
		 */
		public static function setRunningCycles( $waiting, $number=NULL ) {

			self::$waiting = $waiting;

			if ( $number && ((int) $number ) > 0 )
				self::$number_of_cycles = (int) $number;
		}

		/**
		 * Sets new value for the propertie "$number_of_tests"
		 * 
		 * @param  int 			$number
		 */
		public static function setNumberOfRunningTests( $number ) {

			if ((int) $number > 0 )
				self::$number_of_tests = (int) $number;
		}


	/*** Private Methods **************/

		/**
		 * Increases the average time
		 * 
		 */
		private static function runCycles( $func_or_method ) {
			
			$x = 0;
			for ( $i = 0; $i < self::$number_of_cycles; $i++ ) {

				$func_or_method;
			}
		}

		/**
		 * Runs method and sets datas
		 * 
		 */
		private static function getMeasurements( $func_or_method, $i, $cont ) {

			$time_1   = microtime( TRUE );
			$memory_1 = memory_get_usage();

			//Call to tested method / function
			if ( self::$waiting )

				self::runCycles( $func_or_method );
			else
				$func_or_method;

				
			$time_2   = microtime( TRUE );
			$memory_2 = memory_get_usage();

			$memory = ( $memory_2 - $memory_1 );
			$time   = ( $time_2 - $time_1 ) / 1000;

			self::$arr_neccesary_times[ $i ][ $cont ] = $time;
			self::$arr_memory_usages[ $i ][ $cont ]   = $memory;
		}

} //class
