<?php namespace ironwoods\tools\ptester\test;
/**
 * @file: test_performancetest.php
 * @info: File with test for class 'PerformanceTest'
 * 
 * 
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.pTester
 */
 
/*******************************************************************************
 * Test class methods
 * 
 */
use \ironwoods\tools\ptester\classes\PerformanceTest as PerformanceTest;
use \ironwoods\tools\ptester\PTester as PTester;

prob( "<h3>Testing methods from PerformanceTest...</h3>" );


use \ironwoods\tools\ptester\test\vars\X as Xxx;

/**
 * Runs tests
 * 
 */
$x = new Xxx();
$arr_to_test = [
	//Test with an instance
	$x->withIfElse(),
	$x->withSwitch(),

	//Test static methods
	Xxx::useIfElse(),
	Xxx::useSwitch(),

	//Test of functions
	useIfElse(),
	useSwitch(),
	withArgs( 12, 8 ),
];

PTester::test( $arr_to_test );
/**/
prob( "<hr>" );
//Config number of executions (default 100)
PTester::setNumberOfRunningTests( 2000 );
PTester::test( $arr_to_test );
/**/
prob( "<hr>" );
//Config waiting executions (default FALSE / 1000 cycles)
PTester::setRunningCycles( TRUE );
PTester::test( $arr_to_test );

/**
 * Runs individual tests
 * 
 */

PTester::test(
	[ 
		$x->withIfElse()
	]
);
PTester::test(
	[ 
		$x->withSwitch()
	]
);
PTester::test(
	[ 
		Xxx::useIfElse()
	]
);
PTester::test(
	[ 
		Xxx::useSwitch()
	]
);
PTester::test(
	[ 
		useIfElse() 
	]
);
PTester::test(
	[ 
		useSwitch()
	]
);
PTester::test(
	[ 
		withArgs( 12, 8 )
	]
);
PTester::test(
	[ 
		repeatCall( 500 )
	]
);

/**/
