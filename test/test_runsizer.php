<?php namespace ironwoods\tools\ptester\test;
/**
 * @file: test_runsizer.php
 * @info: File with test for class 'Xxx'
 * 
 * 
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.pTester
 */
 
/*******************************************************************************
 * Test class methods
 * 
 */
use \ironwoods\tools\ptester\classes\RunSizer as RunSizer;
use \ironwoods\tools\ptester\PTester as PTester;

prob( "<h3>Testing methods from RunSizer...</h3>" );


withArgs( 1000, 5000 );
PTester::beginSizer();
repeatCall();
PTester::stopSizer();

$stats = PTester::getSizerStats();
prob( $stats );


PTester::beginSizer();
repeatCall();
useIfElse();
PTester::stopSizer();

$stats = PTester::getSizerStats();
prob( $stats );


prob( "Repeat lasts methods two times" );
PTester::beginSizer();
repeatCall();
useIfElse();
repeatCall();
useIfElse();
PTester::stopSizer();

$stats = PTester::getSizerStats();
prob( $stats ); //double time, but the same memory usage (functions in memory ?)
die();



/**/
