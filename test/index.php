<?php namespace ironwoods\tools\ptester\test;
/**
 * @file: index.php
 * @info: Tests for the librarie
 *
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package	ironwoods.tools.pTester
 */

//Report of errors
error_reporting(E_ALL);
ini_set("display_errors", 1);

/*******************************************************************************
 * Load files
 * 
 */
require "../ptester.php";

$test_class		= 'vars/x.php';			//File with class to test
$test_functions = 'vars/functions.php';	//File with functions to test

require BASEPATH_TOOL__PTESTER . "test/" . $test_class;
require BASEPATH_TOOL__PTESTER . "test/" . $test_functions;


/*******************************************************************************
 * Runs tests
 * 
 */
print_styles();

//require "test_datamarks.php";
echo( "<hr><hr>" );


//require "test_intervalsizer.php";
echo( "<hr><hr>" );


//require "test_performancetest.php";
//die( "<hr><hr>" );


require "test_runsizer.php";
echo( "<hr><hr>" );


require "test_utils.php";
echo( "<hr><hr>" );