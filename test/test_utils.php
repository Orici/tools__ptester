<?php namespace ironwoods\tools\ptester\test;
/**
 * @file: test_utils.php
 * @info: File with test for class 'Utils'
 * 
 * 
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.pTester
 */
 

/*******************************************************************************
 * Test class methods
 * 
 */
use \ironwoods\tools\ptester\classes\Utils as Utils;

prob( "<h3>Testing methods from Utils...</h3>" );


$arr_numbers = [ 10, 50 ];
$memory_bytes = Utils::getAvailableMemory();

prob( Utils::getAverage( $arr_numbers )); //30
prob( "Results from \"getAvailableMemory()\": " . $memory_bytes );
prob( Utils::getValueAndUnits( $memory_bytes ));

/**/
