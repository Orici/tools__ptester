<?php namespace ironwoods\tools\ptester\test;
/**
 * @file: test_class_name.php
 * @info: File with test for class 'Xxx'
 * 
 * 
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.pTester
 */
 
/*******************************************************************************
 * Test class methods
 * 
 */
use \ironwoods\tools\ptester\classes\Xxx as Xxx;

prob( "<h3>Testing methods from Xxx...</h3>" );


/**/
