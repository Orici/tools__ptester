<?php namespace ironwoods\tools\ptester\test;
/**
 * @file: test_datamarks.php
 * @info: File with test for class 'DataMarks'
 * 
 * 
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.pTester
 */
 
/*******************************************************************************
 * Test class methods
 * 
 */
use \ironwoods\tools\ptester\classes\DataMarks as DataMarks;

prob( "<h3>Testing methods from DataMarks...</h3>" );

//Delete contents in $GLOBALS 
unset( $GLOBALS );

$res = DataMarks::getFirstTime();
prob( "First time mark: " . $res );

prob( "Initializing array for datas..." );
DataMarks::init();
dx( $GLOBALS );


prob( "Sets datas..." );
DataMarks::setDatas();
dx( $GLOBALS );

sleep( 1 ); //stopped 1 milisecond

prob( "Sets datas..." );
DataMarks::setDatas();
dx( $GLOBALS );

prob( "Shows memory diff..." );
$memory_diff = DataMarks::getMemoryDiff();
prob( $memory_diff . " bytes" );

prob( "Shows time marks..." );
$time_1 = DataMarks::getFirstTime();
$time_2 = DataMarks::getLastTime();
prob( "First time mark: " . $time_1 );
prob( "Last time mark: " . $time_2 );

/**/

prob( "<hr>" );
prob( "<h3>Testing DataMarks::getMemoryDiff()...</h3>" );

prob( "Initializing array for datas..." );
DataMarks::init();

prob( "Sets datas..." );
DataMarks::setDatas();
//dx( memory_get_usage( ));

prob( "Runs a script to increase memory usage..." );
repeatCall();

prob( "Sets datas..." );
DataMarks::setDatas();
//dx( memory_get_usage( ));

prob( "Shows memory diff..." );
$memory_diff = DataMarks::getMemoryDiff();
prob( $memory_diff . " bytes" );