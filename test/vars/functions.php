<?php

function repeatCall( $n=500 ) {
	//prob( '<b>repeatCall()</b>' );

	for ( $i = 0; $i < $n; $i++ ) {

		withArgs( 1000, 5000 );
		useIfElse();
	}
}

function useIfElse( $str='hola' ) {
	//prob( '<b>Probando con if elseif else</b>' );
	
	$x = 'x';
	$y = NULL;
	$z = 12344;
	$a = 'xs';
	$b = FALSE;
	$c = time();
	$res_1 = 'encontrada coincidencia en 1';
	$res_2 = 'encontrada coincidencia en 2';
	$res_3 = 'encontrada coincidencia en 3';
	$res_4 = 'encontrada coincidencia en 4';
	$res_5 = 'encontrada coincidencia en 5';
	$res_6 = 'encontrada coincidencia en 6';
	
	if ($str===$x) {
		(int)$res_1;
		
	} elseif ($str===$y) {
		(int)$res_2;
		
	} elseif ($str===$z) {
		(int)$res_3;
	
	} elseif ($str===$a) {
		(int)$res_4;
		
	} elseif ($str===$b) {
		(int)$res_5;
		
	} elseif ($str===$c) {
		(int)$res_6;	
		
	} else
		(int)'no encontrada coincidencia';
}

function useSwitch( $str='hola' ) {
	//echo '<b>Probando con switch</b><br>';
	
	$x = 'x';
	$y = NULL;
	$z = 12344;
	$a = 'xs';
	$b = FALSE;
	$c = time();
	$res_1 = 'encontrada coincidencia en 1';
	$res_2 = 'encontrada coincidencia en 2';
	$res_3 = 'encontrada coincidencia en 3';
	$res_4 = 'encontrada coincidencia en 4';
	$res_5 = 'encontrada coincidencia en 5';
	$res_6 = 'encontrada coincidencia en 6';
	
	switch($str) {
		case $x: (int)$res_1; break;
		case $y: (int)$res_2; break;
		case $z: (int)$res_3; break;
		case $a: (int)$res_1; break;
		case $b: (int)$res_2; break;
		case $c: (int)$res_3; break;
		default:
			(int)'no encontrada coincidencia'; break;
	}
}

function withArgs( $a, $b ) {

	$res = ( $a * $b ) / $a;

	if ( $a > $b && $res < $b )
		return $res + $a;

	for ( $i = 0; $i < $a; $i++ ) {

		$res++;
	}

	for ( $i = 0; $i < $res; $i++ ) {
		
		$res * 2;
	}
	usleep( $res );
	

	return $res;
}