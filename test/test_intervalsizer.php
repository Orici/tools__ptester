<?php namespace ironwoods\tools\ptester\test;
/**
 * @file: test_intervalsizer.php
 * @info: File with test for class 'IntervalSizer'
 * 
 * 
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.pTester
 */
 
/*******************************************************************************
 * Test class methods
 * 
 */
use \ironwoods\tools\ptester\classes\IntervalSizer as IntervalSizer;
use \ironwoods\tools\ptester\PTester as PTester;

prob( "<h3>Testing methods from IntervalSizer...</h3>" );


//Config -> 10 measurements in 1 second
PTester::setMeasurementsBySecond( 10 );
PTester::setTimeForInterval( 1 );

PTester::beginIntervalSizer();

$x = 0;
for  ( $i = 0; $i < 1000000; $i++ ) {
	
	if ( $i > 0 )
		$x .= ( $i / 2 );
}
PTester::endIntervalSizer();

/**/
