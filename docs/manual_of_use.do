Manual of use / version 0.0.30


1. Load files
	Only the file "ptester.php" is necesary. Example:

		require_once "vendors/ironwoods/pTester/ptester.php";


2. Test a function
	Know stats for a function. Examples:

		//Withour args
		PTester::test(
			[ 
				foo()
			]
		);

		//With args
		PTester::test(
			[ 
				foo2( 666 )
			]
		);

		//Tests both in same time
		PTester::test(
			[
				foo(),
				foo2( 666 )
			]
		);

	The last way is use mainly to compare performance of two different 
implementations of the same behaviour.


3. Test one app
	If the app running begin in one unique point, is posible use the 
above example, but if is neccesary know and interval the methods from 
"IntervalSizer" must be used.
	Implements a new threat and measure and interval of 2 seconds (default):

		PTester::beginIntervalSizer();

	Test and thread finalize when the time for the interval expire, 
and stats are saw.

4. Test of performante for one part of the flow in a running script
	A call to "PTester::beginSizer()" begin the part that will be measure.
	A call to "PTester::stopSizer()" stop the process. 
	stopSizer() can be receive a boolean argument for stop script 
execution and show stats. Is also posible that the script continue and the 
stats will be getted calling to "PTester::getSizerStats()".
	
	Example:

		//Instructions
		//...

		PTester::beginSizer(); //begin measurements

		//Instructions that will be tested
		//...

		PTester::stopSizer( TRUE ); //stop script and show performance stats