<?php
/**
 * @file: libs.php
 * @info: functions
 * 
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * https://www.ironwoods.es
 * 
 * @package	ironwoods.tools.iwtesting
 */
//die( "Loaded libs.php !" );

/**
* Trace / debug
* 
*/
if ( ! function_exists( "prob" )) {

	function prob( $str=NULL ) {
		echo $str . "<br>";
	}
}
	
if ( ! function_exists( "dx" )) {

	function dx( $x=NULL, $finalize=FALSE ) {

		echo "<pre>";
		var_dump( $x );

		if ( $finalize ) {
			die( "</pre><br>" ); 
			exit();
		}
		echo( "</pre><br>" );
	}
}
	

/**
* Others
* 
*/

if ( ! function_exists( "print_styles" )) {

	function print_styles() {
		
		echo '<meta charset="UTF-8">';
		echo "<style>";
		echo "* { margin: 0; padding: 0 }";
		echo "br { margin: 20px 0 10px }";
		echo "h1,h2,h3,h4 { font-size: 1.4em; margin: 0 0 -5px }";
		echo "hr { margin: 15px 0 }";
		echo "p.test { margin: 15px 0 0 }";
		echo ".blue { color:blue }";
		echo ".green { color:green }";
		echo ".red { color:red }";
		echo "</style>";
	}
}
