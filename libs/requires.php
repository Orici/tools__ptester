<?php
/**
 * @file: requires.php
 * @info: Loads the files
 *
 * 
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 * 
 * @package	ironwoods.tools.pTester
 */

//define( "BASEPATH_TOOL__CODEANALIZER", dirname( __FILE__, 2 ) . "/" ); //Only PHP 7
define( "BASEPATH_TOOL__PTESTER", dirname( dirname( __FILE__ )) . "/" );
//die( BASEPATH_TOOL__PTESTER );

/**
 * Main files
 * 
 */
	require BASEPATH_TOOL__PTESTER . "settings/settings.php";
	require BASEPATH_TOOL__PTESTER . "libs/libs.php";


/**
 * Class files
 * 
 */
	require BASEPATH_TOOL__PTESTER . "classes/datamarks.php";
	require BASEPATH_TOOL__PTESTER . "classes/intervalsizer.php";
	require BASEPATH_TOOL__PTESTER . "classes/performancetest.php";
	require BASEPATH_TOOL__PTESTER . "classes/runsizer.php";
	require BASEPATH_TOOL__PTESTER . "classes/stats.php";
	require BASEPATH_TOOL__PTESTER . "classes/utils.php";
