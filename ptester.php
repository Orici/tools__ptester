<?php namespace ironwoods\tools\ptester;
/**
 * @file: ptester.php
 * @info: Main class for the librarie - Tests of performance in PHP
 *
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package	ironwoods.tools.pTester
 */

require_once "libs/requires.php";

use \ironwoods\tools\ptester\classes\IntervalSizer as IntervalSizer;
use \ironwoods\tools\ptester\classes\PerformanceTest as PerformanceTest;
use \ironwoods\tools\ptester\classes\RunSizer as RunSizer;


final class PTester {

	/**************************************************************************/
	/*** Properties declaration ***********************************************/

		private $class = "PTester";


	/**************************************************************************/
	/*** Methods declaration **************************************************/

	/*** Public Methods ***************/

	////////////////////////////////////////////////////////////////////
	/// IntervalSizer
	///

		/**
		 * Begins the time interval
		 * 
		 */
		public static function beginIntervalSizer() {

			IntervalSizer::begin();
		}

		/**
		 * Ends the time interval and show the stats of performance
		 * 
		 */
		public static function endIntervalSizer() {

			IntervalSizer::end();
		}

		/**
		 * Sets new value for the propertie "$cycles"
		 * 
		 * @param int 		$number
		 */
		public static function setMeasurementsBySecond( $number ) {

			IntervalSizer::setMeasurementsBySecond( $number );
		}

		/**
		 * Sets new value for the propertie "$waiting"
		 * 
		 * @param int 		$seconds
		 */
		public static function setTimeForInterval( $seconds ) {

			IntervalSizer::setTime( $seconds );
		}


	////////////////////////////////////////////////////////////////////
	/// PerformanceTest
	/// 
	
		/**
		 * Sets new value for the properties "$waiting" and "$number_of_cycles"
		 * 
		 * @param  boolean		$waiting
		 * @param  int 			$number
		 */
		public static function setRunningCycles( $waiting, $number=NULL ) {

			PerformanceTest::setRunningCycles( $waiting, $number );
		}
		
		/**
		 * Sets new value for the propertie "$number_of_tests"
		 * 
		 * @param  int 			$number
		 */
		public static function setNumberOfRunningTests( $number ) {

			PerformanceTest::setNumberOfRunningTests( $number );
		}

		/**
		 * Tests memory usage and running time for the functions or methods
		 * 
		 * @param  array		$arr_func_or_methods
		 * @param  boolean		$waiting
		 */
		public static function test( $arr_func_or_methods ) {

			PerformanceTest::test( $arr_func_or_methods );
		}


	////////////////////////////////////////////////////////////////////
	/// RunSizer
	///

		/**
		 * Sets initial datas
		 *
		 */
		public static function beginSizer() {

			RunSizer::begin();
		}

		/**
		 * Gets stats
		 *
		 * @return string
		 */
		public static function getSizerStats() {

			return RunSizer::getStats();
		}

		/**
		 * Sets final datas
		 *
		 * @param  boolean		$show_stats
		 */
		public static function stopSizer( $show_stats=FALSE ) {

			RunSizer::stop( $show_stats );
		}


	/*** Private Methods **************/



} //class
